package com.ill_j.coffee.service;

import com.ill_j.coffee.model.Item;
import com.ill_j.coffee.model.ItemType;

import java.util.List;

public class BonusProgram {
    
    public static boolean isFiveBeverageInReceipt(List<Item> itemList) {
        int beverageCount = 0;
        for(Item item : itemList) {
            if(item.getProduct().getItemType().equals(ItemType.BEVERAGE)) {
                beverageCount++;
            }
        }
        return beverageCount >= 5;
    }
    
    // I'm sorry, bonus program under construction.
}
