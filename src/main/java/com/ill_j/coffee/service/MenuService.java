package com.ill_j.coffee.service;

import com.ill_j.coffee.model.ItemType;
import com.ill_j.coffee.model.Product;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
@Data
public class MenuService {
    
    private List<Product> products;
    
    private static final String CURRENCY = "CHF";
    
    public MenuService() {
        products = new ArrayList();
        products.add(new Product(1L, "small Coffee", new BigDecimal(2.5), ItemType.BEVERAGE));
        products.add(new Product(2L, "medium Coffee", new BigDecimal(3), ItemType.BEVERAGE));
        products.add(new Product(3L, "large Coffee", new BigDecimal(3.5), ItemType.BEVERAGE));
        products.add(new Product(4L, "Bacon roll", new BigDecimal(4.5), ItemType.SNACK));
        products.add(new Product(5L, "Freshly squeezed orange juice (0.25l) ", new BigDecimal(3.95), ItemType.BEVERAGE));
        
        products.add(new Product(6L, "Extra milk", new BigDecimal(0.3), ItemType.EXTRA));
        products.add(new Product(7L, "Foamed milk", new BigDecimal(0.5), ItemType.EXTRA));
        products.add(new Product(3L, "Special roast coffee", new BigDecimal(0.9), ItemType.EXTRA));
    }
}
