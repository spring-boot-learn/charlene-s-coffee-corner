package com.ill_j.coffee.service;

import com.ill_j.coffee.model.Item;
import com.ill_j.coffee.model.Product;
import com.ill_j.coffee.model.Receipt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Optional;

@Component
public class CashDeskService {
    
    private Receipt receipt;

    private static final Logger log = LoggerFactory.getLogger(CashDeskService.class);
    
    public Receipt getReceipt() {
        if (receipt == null) {
            newReceipt();
        }
        recountAll(receipt);
        return receipt;
    }
    
    public Receipt getPrintableReceipt() {
        Receipt receipt = getReceipt();
        printReceipt(receipt);
        return receipt;
    }

    /**
     * Recount total price after quantity change
     * @param receipt
     */
    private void recountAll(Receipt receipt) {
        BigDecimal total = new BigDecimal(0);
        for (Item item : receipt.getItems()) {
            if(item.getProduct() == null) {
                log.warn("Receipt item has NULL product.");
                continue;
            }
            total = total.add(recountItem(item));
        }
        receipt.setTotalPrice(total);
    }

    private BigDecimal recountItem(Item item) {
        BigDecimal totalItemPrice = item.getProduct().getItemPrice().multiply(BigDecimal.valueOf(item.getQty()));
        item.setTotalItemPrice(totalItemPrice);
        return totalItemPrice;
    }

    public void newReceipt() {
        receipt = new Receipt();
    }
    
    public Item addItemToReceipt(Product product, int addQuantity) {
        Item itemObject = null;
        Optional<Item> itemOptional = getReceipt().getItems().stream().filter(item -> item.getProduct().getId().equals(product.getId())).findFirst();
        if (itemOptional.isPresent()) {
            itemObject = itemOptional.get();
            itemObject.setQty(itemOptional.get().getQty() + addQuantity);
        } else {
            itemObject = new Item(product, addQuantity, new BigDecimal(0));
            getReceipt().getItems().add(itemObject);
        }
        recountItem(itemObject);
        return itemObject;
    }
    
//    public static void main(String[] args) {
//        new CashDeskService().printReceipt();
//    }
    
    private String printReceipt(Receipt receipt) {
        String columnNames = String.format("%-25s %5s %10s\n", "Item", "Qty", "Price");
        String emptyLine = String.format("%-25s %5s %10s\n", "----", "---", "-----");
        
        StringBuilder receiptSB = new StringBuilder(columnNames + emptyLine);

        for(Item item  : receipt.getItems()){
            String itemLine = String.format("%-25s %5d %10.2f\n", item.getProduct().getName(), item.getQty(), item.getTotalItemPrice());
            receiptSB.append(itemLine);
        }
        String priceTotalLine = String.format("%-25s %5s %10s\n", "TOTAL", "---", receipt.getTotalPrice());
        receiptSB.append(priceTotalLine);
        
        System.out.print(receiptSB);
        return receiptSB.toString();
    }
}
