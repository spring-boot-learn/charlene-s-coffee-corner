package com.ill_j.coffee.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
public class Receipt {
    
    private Long id;
    
    private List<Item> items = new ArrayList();
    
    private BigDecimal totalPrice;
}
