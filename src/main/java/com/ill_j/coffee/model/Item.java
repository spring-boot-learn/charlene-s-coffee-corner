package com.ill_j.coffee.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class Item {
    
    private Product product;
    
    private int qty;
    
    private BigDecimal totalItemPrice;
}
