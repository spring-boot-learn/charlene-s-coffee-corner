package com.ill_j.coffee.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class Product {
    
    private Long id;
    
    private String name;
    
    private BigDecimal itemPrice;
    
    private ItemType itemType;
}
