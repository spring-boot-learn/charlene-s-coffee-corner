package com.ill_j.coffee.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ItemType {
    
    BEVERAGE(1),
    SNACK(2),
    EXTRA (3);
    
    private int id;
}
