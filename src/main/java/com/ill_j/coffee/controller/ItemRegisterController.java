package com.ill_j.coffee.controller;

import com.ill_j.coffee.model.Item;
import com.ill_j.coffee.model.Product;
import com.ill_j.coffee.model.Receipt;
import com.ill_j.coffee.service.CashDeskService;
import com.ill_j.coffee.service.MenuService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

import java.util.List;

@Api(tags = { "CoffeeCorner" }, value = "API for list of goods and download receipt.")
@RestController
@RequestMapping(path = "/api/v1/coffeecorner" )
public class ItemRegisterController {
    
    private CashDeskService cashDeskService;
    private MenuService menuService;

    private static final Logger log = LoggerFactory.getLogger(ItemRegisterController.class);
    
    @Autowired
    public ItemRegisterController(CashDeskService cashDeskService, MenuService menuService) {
        this.cashDeskService = cashDeskService;
        this.menuService = menuService;
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Return Say hello", responseHeaders = @ResponseHeader(name = "Nice day", description = "description TBD", response = ResponseEntity.class)),
            @ApiResponse(code = 204, message = "not found."),
            @ApiResponse(code = 401, message = "Unauthorized access."),
            @ApiResponse(code = 500, message = "Internal server error") })
    @GetMapping(value = "", produces = {MediaType.ALL_VALUE})
    public String hello()
    {
        return "Hello from Charlene's Coffee Corner";
    }
    
    // get menu
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Return caffee offer"),
            @ApiResponse(code = 500, message = "Internal server error") })

    @ApiOperation(value = "Get list of drinks and snack to buy. ", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping(value = "/menu", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<Product>> getMenu() {

        return new ResponseEntity<List<Product>>(menuService.getProducts(), HttpStatus.OK);
    }
    
    // new receipt
    // TODO:
    
    // add Item
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Return added item", 
                    responseHeaders = @ResponseHeader(name = "Added item", description = "Cash desk added new item or increase quantity", response = ResponseEntity.class)),
            @ApiResponse(code = 204, message = "not found."),
            @ApiResponse(code = 401, message = "Unauthorized access."),
            @ApiResponse(code = 500, message = "Internal server error") })
    @ApiOperation(value = "Add product and its quantity to receipt.", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping(path= "/add", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Item> addItem(
            @ApiParam(name = "product object", value = "enter one from menu", example = "{\n" +
                    "    \"id\": 1,\n" +
                    "    \"name\": \"small Coffee\",\n" +
                    "    \"itemPrice\": 2.5,\n" +
                    "    \"itemType\": \"BEVERAGE\"\n" +
                    "  }", required = true ) 
            @RequestBody Product product, 
            @RequestParam(value = "quantity of goods")  int qty) {
        Item item = cashDeskService.addItemToReceipt(product, qty);
        
        return new ResponseEntity<Item>(item, HttpStatus.OK);
    }
    
    // print receipt 
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Receipt printed in terminal"),
            @ApiResponse(code = 500, message = "Internal server error") })

    @ApiOperation(value = "Recount and print all items on receipt.", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping(value = "/receipt", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Receipt> getReceipt() {

        return new ResponseEntity<Receipt>(cashDeskService.getPrintableReceipt(), HttpStatus.OK);
    }
}
