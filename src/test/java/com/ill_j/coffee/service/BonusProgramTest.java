package com.ill_j.coffee.service;

import com.ill_j.coffee.model.Item;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class BonusProgramTest {
    
    private MenuService menuService = new MenuService();
    
    @Test
    void fiveBeverageTest() {
        List<Item> items = new ArrayList();
        Item item1 = new Item(menuService.getProducts().get(0), 4, BigDecimal.ZERO);
        items.add(item1);
        BonusProgram.isFiveBeverageInReceipt(items);
        // TODO: assertion
    }
}
